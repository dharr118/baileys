"""
Module for storing ice cream flavors
"""
from django.db import models

FLAVOR_CATEGORY = (
    ('Premium Ice Cream', 'Premium Ice Cream'),
    ('95% Fat Free Yogurt', '95% Fat Free Yogurt'),
    ('Low Fat (95% Fat Free) No Sugar Added Ice Cream', 'Low Fat (95% Fat Free) No Sugar Added Ice Cream'),
    ('Sherbet', 'Sherbet'),
    ('Sorbet (fat-free and non-dairy)', 'Sorbet (fat-free and non-dairy)')
)


class Flavor(models.Model):
    """
    flavors model
    """
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=2000, blank=True)
    available = models.BooleanField(default=True)
    category = models.CharField(max_length=50, choices=FLAVOR_CATEGORY)

    def __str__(self):
        return self.name
