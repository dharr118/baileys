from django.contrib import admin
from .models import Flavor


class FlavorAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'available')


admin.site.register(Flavor, FlavorAdmin)
