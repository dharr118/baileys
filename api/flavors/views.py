from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination

from .models import Flavor
from .serializers import FlavorSerializer


class FlavorViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A simple ViewSet for viewing flavors.
    """
    queryset = Flavor.objects.filter(available=True)
    serializer_class = FlavorSerializer
    pagination_class = PageNumberPagination
