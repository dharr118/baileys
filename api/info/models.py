"""
Module for storing various site info
"""
# built-in
import json
from enum import Enum

# 3rd party
from django.db import models

VALUE_TYPES = Enum('VALUE_TYPES', [
    'JSON', 'LIST', 'STRING'
])

INFO_TYPE = (
    (VALUE_TYPES.JSON.name, 'JSON'),
    (VALUE_TYPES.LIST.name, 'List'),
    (VALUE_TYPES.STRING.name, 'String')
)


class Info(models.Model):
    """
    info model
    """
    key = models.CharField(max_length=255)
    value = models.CharField(max_length=2000)
    value_type = models.CharField(max_length=20, choices=INFO_TYPE)
    description = models.CharField(max_length=255)

    def load_value(self):
        """
        Loads value, serializes value if type is LIST or JSON.
        """
        if (self.value_type == VALUE_TYPES.JSON.name or
                self.value_type == VALUE_TYPES.LIST.name):
            return json.loads(self.value)
        return self.value

    def set_value(self, value):
        """
        Sets value, dumps JSON string if type is LIST or JSON. Otherwise,
        convert it to string.
        """
        if (self.value_type == VALUE_TYPES.JSON.name or
                self.value_type == VALUE_TYPES.LIST.name):
            self.value = json.dumps(value)
        else:
            self.value = str(value)
