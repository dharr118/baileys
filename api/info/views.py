from rest_framework import viewsets

from .models import Info
from .serializers import InfoSerializer


class InfoViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A simple ViewSet for viewing store info.
    """
    queryset = Info.objects.filter()
    serializer_class = InfoSerializer
