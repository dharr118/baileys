from django.contrib import admin
from .models import Info


class InfoAdmin(admin.ModelAdmin):
    list_display = ('key', 'value', 'value_type')


admin.site.register(Info, InfoAdmin)
