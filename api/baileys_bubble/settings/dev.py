# Dev specific settings

from .base import *  #noqa

DEBUG = True
SECRET_KEY = '22-h*x_9o-n&fi4*&&-hr5&vm9w%tl-#pv3wj57)aqs)dcsx(l'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'baileys',
        'USER': 'baileys',
        'PASSWORD': 'changeme',
        'HOST': 'db',
        'PORT': '5432',
    }
}

CORS_ORIGIN_WHITELIST = (
    'dev.baileysbubble.com'
)
