"""
baileys_bubble URL Configuration
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers

from flavors.views import FlavorViewSet

router = routers.DefaultRouter()
router.register(r'flavors', FlavorViewSet, 'flavors')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^api/', include(router.urls, namespace="api")),
]
