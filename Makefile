# -------------------------
# Docker
# -------------------------
DC=docker-compose
EXEC=docker exec

up:
	$(DC) up

build:
	$(DC) build --no-cache

# -------------------------
# NGINX
# -------------------------
NGINX_INSTANCE=1
NGINX_EXEC=docker exec baileysbubble_nginx_$(NGINX_INSTANCE)

nginx-reload:
	$(NGINX_EXEC) service nginx reload


# -------------------------
#  API Development Commands
# -------------------------
API_INSTANCE=1
API_EXEC=$(EXEC) baileysbubble_api_$(API_INSTANCE)
API_MANAGE_PY=$(API_EXEC) python manage.py

SU_USERNAME=admin
SU_EMAIL=admin@admin.com
SU_PASSWORD=admin

cd-api:
	cd api

make-migrations: cd-api
	$(API_MANAGE_PY) makemigrations $(APP);

migrate: cd-api
	$(API_MANAGE_PY) migrate;

collect-static: cd-api
	$(API_MANAGE_PY) collectstatic --noinput

restart: cd-api
	$(API_EXEC) uwsgi --reload /tmp/baileys_bubble-master.pid;

reset-db: cd-api
	$(API_MANAGE_PY) flush --no-input;\
	$(MAKE) migrate;\
	$(MAKE) load-data;

load-data: cd-api
	$(API_MANAGE_PY) loaddata --app=flavors initial_data.json;\
	$(API_MANAGE_PY) loaddata --app=info initial_data.json

create-superuser:
	export SU_USERNAME=$(SU_USERNAME) &&\
	export SU_EMAIL=$(SU_EMAIL) &&\
	export SU_PASSWORD=$(SU_PASSWORD) &&\
	./scripts/create-django-superuser.sh


# -------------------------
# UI Development Commands
# -------------------------
watch:
	cd ui && gulp dev
