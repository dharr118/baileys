(function () {
  'use strict';

  var app = angular.module('baileys.navbar');

  app.directive('navbar', Navbar);

  function Navbar() {
    return {
      scope: {},
      restrict: 'E',
      templateUrl: '/js/navbar/navbar.html'
    }
  }
})();
