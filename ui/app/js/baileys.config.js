(function() {
  'use strict';

  angular.module('baileys.config', []);
  angular.module('baileys.config').config(config);

  // Enable HTML5 Routing
  function config($locationProvider) {
    $locationProvider.html5Mode(true);
    // $locationProvider.hashPrefix('!');
  }
})();
