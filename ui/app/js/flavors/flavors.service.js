(function () {
  'use strict';

  var app = angular.module('baileys.flavors');
  app.factory('FlavorService', FlavorService);

  var apiRoute = 'flavors/?format=json';

  function FlavorService($http, $q, config) {

    // FlavorService interface
    var FlavorService = {
      get: get,
      flavors: undefined
    };

    function get() {
      return $http.get(config.API_URL + apiRoute).then(getSuccess, getError);

      function getSuccess(data) {
        FlavorService.flavors = _sortFlavorByCategory(data.data.results);
        console.log(data);
        return FlavorService.flavors;
      }

      function getError(data) {
        console.error("Failed to load flavors");
        throw data;
      }
    }

    function _sortFlavorByCategory(flavorList) {
      var _flavors = {};
      console.log(flavorList);
      flavorList.forEach(function(flavor) {
        if (!_flavors.hasOwnProperty(flavor.category)) {
          _flavors[flavor.category] = [];
        }
        _flavors[flavor.category].push(flavor);
      });

      return _flavors;
    }

    return FlavorService;
  }
})();
