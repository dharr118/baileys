(function () {
  'use strict';

  var app = angular.module('baileys.flavors');
  app.controller('flavorListController', FlavorListController);

  function FlavorListController($scope, $location, FlavorService) {
    var vm = this;
    vm.flavors = [];

    vm.getFlavors = function() {
      if (FlavorService.flavors !== undefined) {
        vm.flavors = FlavorService.flavors;
        return;
      }
      FlavorService.get()
        .then(
          function(data) {
            vm.flavors = data;
            console.log(FlavorService.flavors);
          },
          function(data) {
            console.error('Failed to load flavors');
          }
        );
    };

    if (vm.flavors.length === 0) {
      vm.getFlavors();
    }
  }
})();
