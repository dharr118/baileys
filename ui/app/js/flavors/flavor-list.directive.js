(function () {
  'use strict';

  var app = angular.module('baileys.flavors');
  app.directive('flavorList', FlavorList);

  function FlavorList() {
    return {
      controller: 'flavorListController',
      controllerAs: 'vm',
      restrict: 'E',
      templateUrl: '/js/flavors/flavor-list.html'
    };
  }
})();
