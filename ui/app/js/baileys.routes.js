(function() {
  'use strict';

  angular.module('baileys.routes', ['ngRoute']);

  angular.module('baileys.routes').config(config);

  function config($routeProvider) {
    $routeProvider
    .when('/', {
      templateUrl: '/partials/home.html',
      title: 'home'
    })
    .when('/about-us', {
      templateUrl: '/partials/about-us.html',
      title: 'about-us'
    })
    .when('/flavors', {
      templateUrl: '/partials/flavors.html',
      title: 'flavors'
    })
    .otherwise('/');
  }
})();
