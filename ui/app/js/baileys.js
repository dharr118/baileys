(function() {
  'use strict';

  angular.module('baileys', [
    'baileys.config',
    'baileys.routes',
    'baileys.navbar',
    'baileys.flavors'
  ]);

  var app = angular.module('baileys');
  app.constant('config', {
    'API_URL': 'http://api.david-harrigan.com/api/'
  });

  angular.module('baileys').run(run);

  function run($http) {
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
  }
})();
